<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\ConsultasController;
//use App\Http\Livewire\ShowTurnos;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [InicioController::class, 'index']);
Route::get('/quienes/', [InicioController::class, 'quienes']);
Route::get('/contactenos/', [InicioController::class, 'contactenos']);
Route::get('/cpms/', [InicioController::class, 'cpms']);
Route::get('/pfpc/', [InicioController::class, 'pfpc']);
Route::get('/turnos/', [InicioController::class, 'turnos']);
Route::get('/blog/', [InicioController::class, 'blog'])->name('blogs');
Route::get('/nota/{blog}', [InicioController::class, 'nota'])->name('nota');
Route::post('cpms/respuesta', [InicioController::class, 'cpmsRespuesta']);
Route::get('/consultas/meppes', [ConsultasController::class, 'meppes']);
Route::get('/consultas/eventos', [ConsultasController::class, 'eventos']);
Route::post('/consultas/respuesta', [ConsultasController::class, 'meppesRespuesta']);
Route::get('/consultas/vacunatorio', [ConsultasController::class, 'vacunatorio']);

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\admin\HomeController;
use App\Http\Controllers\admin\NovedadeController;
use App\Http\Controllers\admin\BlogController;
use App\Http\Controllers\admin\TurnoController;
use App\Http\Controllers\admin\OfertaController;



Route::get('', [HomeController::class, 'index']);
Route::Resource('blog', BlogController::class)->names('admin.blog');
Route::Resource('novedades', NovedadeController::class)->names('admin.novedades');
Route::Resource('turnos', TurnoController::class)->names('admin.turnos');
Route::Resource('ofertas', OfertaController::class)->names('admin.ofertas');


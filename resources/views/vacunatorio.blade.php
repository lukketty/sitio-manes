@extends('layouts.app')

@section('resto')

<section class="well1 ins3">
    <div class="container">
      <p class=titulo4>Vacunatorio Manes</h1>
      <div class="row off1">
        <div class="grid_6">
          <p class="titulo1">¿Qué es el Vacunatorio Manes?</p>
          <br><p class="parrafo2">Manes ofrecerá dentro de su farmacia un espacio acondicionado para la aplicación de vacunas y medicación inyectable.</p><p class="parrafo2">Este servicio gratuito podrá ser programado con anticipación via web solicitando un turno (Opción disponible para todo paciente dentro del Programa Farmacéutico para Patologías Crónicas PF)</p><p class="parrafo3">Atención: El Vacunatorio Manes <u>no realizará</u> aplicaciones de antibióticos betalactácmicos.</p>
          <hr>
          <p class="titulo1">Aplicaciones</p>
          <div class="row">
            <div data-wow-delay="0.2s" class="grid_3 wow fadeInLeft"><img src="{{url('/')}}/images/farma.jpg" alt=""></div>
            <div class="grid_3 wow fadeInLeft"><img src="{{url('/')}}/images/enfermera.jpg" alt=""></div>
          </div>
          <p class="parrafo2">Un enfermero y personal capacitado se encontrará disponible para realizar dicha aplicación a aquellas personas que han retirado de Manes el producto. Una vez realizada la aplicación, se entregará el correspondiente certificado al paciente.</p>
        </div>
        <div class="grid_6">
          <p class="titulo1">Calendario vacunación: ¿Quiénes deben vacunarse?</p>
          <div class="row">
            <div class="grid_2 wow fadeInRight"><img src="{{url('/')}}/images/embarazadas.jpg" alt=""><img src="{{url('/')}}/images/posparto.jpg" alt=""></div>
            <div data-wow-delay="0.2s" class="grid_2 wow fadeInRight"><img src="{{url('/')}}/images/baby.jpg" alt=""><img src="{{url('/')}}/images/ninos.jpg" alt=""></div>
            <div data-wow-delay="0.4s" class="grid_2 wow fadeInRight"><img src="{{url('/')}}/images/adultos.jpg" alt=""><img src="{{url('/')}}/images/+65.jpg" alt=""></div>
          </div>
          <p class="parrafo2">El calendario de vacunación no sólo contempla aquellas vacunas que deben recibir los infantes, sino también aquellas destinadas a adultos, embarazadas, mujeres en estado de posparto/posaborto y adultos mayores a 65 años de edad.</p>
        </div>
        <div class="grid_6">
        <br><hr>
          <div class="row">
          </div>
          <p class="cartel1"></p>
        </div>
      </div>
    </div>
  </section>
  <section class="well1 ins3 bg-primary">
    <div class="container">
      <p class="titulo3">¿Qué vacunas aplicamos?</p>
      <br><br><ul class="product-list row off1">
        <li class="grid_12">
          <div class="box">
            <div class="box_aside">
              <div class="icon fa-adjust"></div>
            </div>
            <div class="box_cnt__no-flow">
              <h3>Antigripal - Diversas</h3>
              <p>ACCION: Inmunización activa contra los virus de influenza tipos A y B. <br> PRESENTACIONES: Envase conteniendo 1 jeringa prellenada con monodosis por 0.5 ml.</p>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </section>
  <section class="well1">
    <div class="container">
      <div class="row">
        <div class="grid_12">
          <p class="cartel1">Antibióticos Betalactámicos</p>
          <p class="parrafo4">El Vacunatorio de Manes <u>no realiza</u> aplicaciones de antibióticos betalactámicos(Ejemplo: Penicilina).</p>
        </div>
      </div>
    </div>
  </section>
@endsection

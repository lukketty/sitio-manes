@extends('layouts.app')
@section('banner')
<section class="camera_container">
    <div id="camera" class="camera_wrap">
     <div data-src="./images/front_1.jpg">
        <div class="camera_caption fadeIn">
          <div class="container">
            <div class="row">
              <div class="preffix_6 grid_6" style="margin-top: 150px"><p class="titulo1" > Pograma Farmacéutico:</p>
                                             <p class="titulo2" >Renovando la atención farmacéutica</p>
                                             <p class="titulo3" >Una nueva perspectiva en el ámbito farmaceútico</p> 
                                             <br><div align="center"><a href="{{url('/')}}/site/quienes/redpfpc" class="titulo13"><b>¿Querés sumarte a la red del Programa Farmacéutico Manes? <br> Laboratorios & Farmacias. Clic aquí </b></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div data-src="./images/front_3.jpg">
        <div class="camera_caption fadeIn">
          <div class="container">
            <div class="row">
              <div class="preffix_6 grid_6" style="margin-top: 150px"> <p class="titulo1" >Servicios integrales:</p>
                                             <p class="titulo2" >Pacientes - Programa Farmacéutico Manes</p>
                                             <p class="titulo3" >Historia clínica on-line y más</p>
                                             <br><br><br><div align="center"><a href="{{url('/')}}/site/quienes/redpfpc" class="titulo13"><b>¿Querés sumarte a la red del Programa Farmacéutico Manes? <br> Laboratorios & Farmacias. Clic aquí </b></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div data-src="./images/front_4.jpg">
        <div class="camera_caption fadeIn">
          <div class="container">
            <div class="row">
              <div class="preffix_6 grid_6" style="margin-top: 150px"> <p class="titulo1" >Cobertura:</p>
                                             <p class="titulo2" >Más de 100 Obras sociales, planes y coseguros</p>
                                             <br><br><br><div align="center"><a href="{{url('/')}}/site/quienes/redpfpc" class="titulo13"><b>¿Querés sumarte a la red del Programa Farmacéutico Manes? <br> Laboratorios & Farmacias. Clic aquí </b></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div data-src="./images/front_5.jpg">
        <div class="camera_caption fadeIn">
          <div class="container">
            <div class="row">
              <div class="preffix_6 grid_6" style="margin-top: 150px"> <p class="titulo1" >Programa Farmacéutico Manes:</p>
                                             <p class="titulo2" >Consultorio Farmacéutico</p>
                                             <p class="titulo3" >Atención personalizada y priorizada</p>
                                             <br><br><br><div align="center"><a href="{{url('/')}}/site/quienes/redpfpc" class="titulo13"><b>¿Querés sumarte a la red del Programa Farmacéutico Manes? <br> Laboratorios & Farmacias. Clic aquí </b></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
       <div data-src="./images/front_6.jpg">
        <div class="camera_caption fadeIn">
          <div class="container">
            <div class="row">
              <div class="preffix_6 grid_6" style="margin-top: 150px"> <p class="titulo1" >Próximamente:</p>
                                             <p class="titulo2" >Manes tienda On-line</p>
                                             <p class="titulo3" >Comprá, pagá con tarjeta, retirá.</p>
                                             <br><br><br><div align="center"><a href="{{url('/')}}/site/quienes/redpfpc" class="titulo13"><b>¿Querés sumarte a la red del Programa Farmacéutico Manes? <br> Laboratorios & Farmacias. Clic aquí </b></a></div> 
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </section>
  <section>
    <!-- WhatsHelp.io widget -->
<script type="text/javascript">
(function () {
  var options = {
      whatsapp: "+542215981987", // WhatsApp number
      call_to_action: "¿Interesado en el Programa Farmacéutico Manes?", // Call to action
      position: "left", // Position may be 'right' or 'left'
  };
  var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
  var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
  s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
  var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
})();
</script>
<!-- /WhatsHelp.io widget -->

    <div class="container banner_wr">
      
      <ul class="banner">
        <li style="cursor: pointer;" onclick="window.location='https://farmaciamanes.mitiendanube.com/';">
          <img src="{{url('/')}}/image/icono-tienda.png" alt="" style="width: 40%">
           <h3>Tienda On-line</h3>
           
         </li>
       
       <!-- 
        <li style="cursor: pointer;" onclick="window.location='{{url('/')}}/turnos';">
          <div class="fa fa-calendar" ></div>
          <h3>Turno on-line</h3> 
          
        </li>
        --!>
       
        <li style="cursor: pointer;" onclick="window.location='{{url('/')}}/blog';">
          <div class="fa fa-newspaper-o"></div>
          <h3>Blogs de Novedades</h3>
          
        </li>
        <li style="cursor: pointer;" onclick="window.location='https://alt.manes.com.ar/pspdiab';">
          <div class="fa fa-bar-chart"></div>
          <h3>Pacientes<br> Prog-Farma</h3>
        </li>
        <li style="cursor: pointer;" onclick="window.location='https://alt.manes.com.ar/psppo';">
          <div class="fa fa-file-text"></div>
          <h3>Operadores<br/>Obras Sociales</h3>
        </li>
        <li style="cursor: pointer;" onclick="window.location='./consultas/meppes';">
          <div class="fa fa-file-text"></div>
          <h3>Planes Especiales<br/>Meppes - Camoyte</h3>
        </li>

      </ul>
    </div>
  </section>
@endsection

@section('eventos')
<section class="well1 ins1">
    <div class="container hr">
    <p class="titulo5" style="text-align: center;">PRÓXIMOS EVENTOS</p>
      
        @forelse ($eventos as $evento)
        @if ($loop->odd)
        <div class="row off2" style="text-align: center;">
        @endif
        
        <div class="grid_6"><img src="{{Storage::url($evento->imagen)}}" alt="{{$evento->descripcion}}">
          <h3>{{$evento->titulo}}</h3>
          <p>{{$evento->contenido}}</p>
          <a href="{{$evento->url}}" class="btn">INSCRIBIRSE</a>
        </div>

        @if ($loop->even)
        </div>
        @endif

        
        @empty
        <div class="row off2" style="text-align: center;">
            <div class="grid_12"><img src="{{url('/')}}/images/sin-eventos.jpg" alt="sin enventos por el momento" style="width: 40%">
             
              <p>No hay eventos proximos </p>
            </div>
        </div>
        @endforelse
    </div>
    <div class="row col-sm-12" style="text-align: center;">
    <a name="vermas" id="vermas" class="btn btn-sm" href="{{url('/')}}/consultas/eventos" role="button">Ver todos</a>
    </div>
  </section>
 
  
    
@endsection
@section('ofertas')
<section class="well1 ins1">
  <div class="container hr">
    <p class="titulo4" style="text-align: center;">Ofertas del mes</p>
      <ul class="row product-list">  
       
      @forelse ($ofertas as $oferta)
         <li class="grid_3" style="margin-top: 20px; text-align:center">
        <div class="box wow fadeInRight ofertas">
          <div class="box_aside">
             <img src="{{Storage::url($oferta->imagen)}}" alt="{{$oferta->descripcion}}" style="width: 100%">
          </div>
          <div class="box_cnt__no-flow">
            <h3>{{$oferta->titulo}}</h3>
            <p>{!!$oferta->contenido!!}</p>
          </div>
        </div>
        
      </li>
      
      @empty
      <div class="row off2" style="text-align: center;">
          <div class="grid_12"><img src="{{url('/')}}/images/sin-ofertas.png" alt="sin enventos por el momento"  width="30%" style="padding-left: 80px;" >
                      
          </div>
      </div>
      @endforelse
      </ul>
  </div>
      
</section>

    
@endsection
@section('publicidad')

<section class="well1 ins1">
    <div class="container hr">
      <div class="row">
        <div class="grid_12">
            <div id="amazingslider-wrapper-1" style="display:block;position:relative;max-width:750px;margin:0px auto 0px;">
                  <div id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
                      <ul class="amazingslider-slides" style="display:none;">
                        
                       @forelse ($fila as $item)
                       @if ($item->tipo=="vid")
                       <li><video preload="none" src="{{$urlPubli}}images/{{$item->url}}" ></video></li>
                             
                       @else
                           
                       <li><img src="{{$urlPubli}}images/{{$item->url}}" alt="{{$item->alt}}" data-description="{{$item->alt}}" title="{{$item->nombre}}" /></li>
                       @endif
                      
                       @empty
                          <h1> No hay nada</h1>
                       @endforelse
                        
                         
                      </ul>
                     
                  <div class="amazingslider-engine"><a href="http://amazingslider.com" title="Responsive JavaScript Image Slideshow">Responsive JavaScript Image Slideshow</a></div>
                  </div>
              </div>
        </div>              
    </div>
    </div>
  </section>
    
@endsection
@section('resto')
<section class="well">
    <div class="container">
      <ul class="row product-list">
        <li class="grid_6">
          <div class="box wow fadeInRight">
            <div class="box_aside">
              <div class="icon fa-envelope-o"></div>
            </div>
            <div class="box_cnt__no-flow">
              <h3><a href="#">¿Querés trabajar con nosotros?</a></h3>
              <p>Deja tu CV en el mostrador y nos contataremos con vos.</p>
            </div>
          </div>
          <hr>
          <div data-wow-delay="0.2s" class="box wow fadeInRight">
            <div class="box_aside">
              <div class="icon fa-check-circle-o"></div>
            </div>
            <div class="box_cnt__no-flow">
              <h3><a href="https://docs.google.com/forms/d/e/1FAIpQLSd2EptZL15lZsbXO1z_RUlPOIi4_GVm-Cp4wZ3gQjAQGZMSBA/viewform?usp=sf_link">Calificá nuestra atención</a></h3>
              <p>Ayudanos a crecer y mejorar nuestra atención para brindarte un mejor servicio. Calificá nuestra atención en farmacia, en nuestro chat, atención telefónica, etc.</p>
            </div>
          </div>
        </li>
        <li class="grid_6">
          <div data-wow-delay="0.3s" class="box wow fadeInRight">
            <div class="box_aside">
              <div class="icon fa-calendar"></div>
            </div>
            <div class="box_cnt__no-flow">
              <h3><a href="http://www.colfarmalp.org.ar/turnos-la-plata/">Farmacia de turno</a></h3>
              <p>¿Necesitás saber qué farmacias se encuentran de turno hoy? Te proveemos la lista de farmacias de La Plata que se encuentran de turnos en el mes.</p>
            </div>
          </div>
          <hr>
          <div data-wow-delay="0.4s" class="box wow fadeInRight">
            <div class="box_aside">
              <div class="icon fa-book"></div>
            </div>
            <div class="box_cnt__no-flow">
              <h3><a href="{{url('/')}}/recertarios/samples/basic/menu.php">Recetarios on-line </a></h3>
              <p>Te acercamos recetas de comida saludable para que descubras la posibilidad de comer rico y sano a la vez. Recetas para diabéticos, celíacos, hipertensos y próximamente muchas más. </p>
            </div>
          </div>
        </li>
      </ul>
      
    </div>
  </section >
    
@endsection

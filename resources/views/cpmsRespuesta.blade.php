@extends('layouts.app')

@section('resto')
<script type="text/javascript">
    $('body').bind('copy paste',function(e) {
e.preventDefault(); return false; 
});
  </script>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script> 
 
    <section class="well1">
      <div class="container">
         <p class="titulo6"><img src="{{url('/')}}images/logocaja.jpg" > Caja de médicos, consulta de vademecum</p>
        <hr>
        <div class="row">
          <div class="grid_12">
            <table class="table table-condensed" style="align-items: center;" id="tablaVademe">
              <thead>
                <tr class="active">
                  <th style="text-align: center">Id</th>
                  <th style="text-align: center">troquel</th>
                  <th style="text-align: center">producto</th>
                  <th style="text-align: center">laboratorio</th>
                  <th style="text-align: center">monodroga</th>
                </tr>
              </thead>
              <tbody class="buscar">
         @if ($resultado)
             @foreach ($respuesta as $val)
             <tr>
                <td>{{$val->id}}</td>
                <td>{{$val->troquel}}</td>
                <td>{{$val->producto}}</td>
                <td>{{$val->laboratorio}}</td>
                <td>{{$val->monodroga}}</td>
                 
               </tr>
                 
             @endforeach
         @else
             <h1>Error no se encontro su DNI en el padron</h1>
         @endif
          </tbody>
        </table>
            
          </div>
        </div>
       
        <hr>
        <script>
                  $(document).ready( function () {
                      $('#tablaVademe').DataTable({
                          language: {
                              "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                          },"bSort": true, 
                          "pageLength": 30
                      });
                  } );
        </script>
        
        
   
      </div>
    </section>
@endsection

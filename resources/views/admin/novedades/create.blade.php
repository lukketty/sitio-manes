@extends('adminlte::page')

@section('title', 'Manes Web panel')


@section('content_header')
    <h1>Crear Evento</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            {!!Form::open(['route'=>'admin.novedades.store', 'autocomplete'=>'off', 'files'=>true])!!}
            {!! Form::hidden('user_id', auth()->user()->id) !!}
            <div class="form-group">
                {!! Form::label('titulo', 'Título') !!}
                {!! Form::text('titulo', null, ['class'=>'form-control', 'placeholder'=>'Ingrese el titulo del Evento']) !!}
                @error('titulo')
                <span class="text-danger">{{$message}}</span>
                    
                @enderror
            </div>
            <div class="row mb-3">
                <div class="col">
                    <div class="imagen-wrapper">
                        <img id="picture"  src="{{url('/')}}/images/sin-imagen.jpg" alt="sin imagen">                    
                    </div>
                </div>
                <div class="col mb-3">
                    <div class="form-group">
                        {!! Form::label('imagen', 'Imagen') !!}
                        {!! Form::file('imagen', ['class'=>'form-inline']) !!}
                        @error('imagen')
                        <span class="text-danger">{{$message}}</span>
                            
                        @enderror
                        <p class="mt-3">
                            Seleccione la Imagen a cargar para el posteo, receuerde que la imagen debe tener un formato de dimensiones especifica, no carge imagenes muy pesadas ni muy grandes, el tamaño para un posteo y una visualizacion adecueada debe ser de 400x500 y no superar los 2MB
                        </p>
                    </div>
                   
                </div>
                
            </div>
            
            <div class="form-group">
                {!! Form::label('contenido', 'Contenido') !!}
                {!! Form::textarea('contenido', null, ['class'=>'form-control']) !!}
                @error('contenido')
                <span class="text-danger">{{$message}}</span>
                    
                @enderror
            </div>
            <div class="form-group">
                {!! Form::label('url', 'Url Formulario Google') !!}
                {!! Form::text('url', null, ['class'=>'form-control', 'placeholder'=>'Ingrese el link del formulario del Evento']) !!}
                @error('url')
                <span class="text-danger">{{$message}}</span>
                    
                @enderror
            </div>
            <div class="form-group">
                {!! Form::label('fecha', 'fecha del evento') !!}
                {!! Form::date('fecha', null, ['class'=>'form-control', 'placeholder'=>'Ingrese la fecha del Evento']) !!}
                @error('fecha')
                <span class="text-danger">{{$message}}</span>
                    
                @enderror
            </div>
           
            {!! Form::submit('Crear Evento', ['class'=>'btn btn-primary']) !!}
            {!!Form::close()!!}

        </div>
    </div>
@stop
@section('css')
<style>
    .imagen-wrapper{
        position: relative;
        padding-bottom: 56%;
    }
    .imagen-wrapper img{
        position: absolute;
        object-fit: cover;
        width: 100%;
        height: 100%
    }
</style>
    
@endsection

@section('js')
    <script>
        document.getElementById("imagen").addEventListener('change', cambiarImagen);

        function cambiarImagen(event){
            var file = event.target.files[0];

            var reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("picture").setAttribute('src', event.target.result);
            };

            reader.readAsDataURL(file);
        }
    </script>
@endsection
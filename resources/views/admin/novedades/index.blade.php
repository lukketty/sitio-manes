@extends('adminlte::page')

@section('title', 'Manes Web panel')

@section('content_header')
    <h1>Eventos</h1>
@stop

@section('content')
@if (session('info'))
    <div class="alert alert-danger">
        {{session('info')}}
    </div>
@endif
    <div class="card">
        <div class="card-header">
           <a href="{{route('admin.novedades.create')}}" class="btn btn-info">crear Item</a> 
        </div>
        <div class="card-body">
            <table class="table table-striped ">
                <thead class="thead-inverse">
                    <tr>
                        <th> Nro </th>
                        <th> usuario </th>
                        <th>fecha</th>
                        <th>Titulo</th>
                        <th>Contenido</th>
                        <th>Url Google</th>
                        <th colspan="2">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse ($novedades as $novedad)

                        <tr>
                            <td scope="row">{{$novedad->id}}</td>
                            <td>{{$novedad->user->name}}</td>
                            <td>{{$novedad->fecha}}</td>
                            <td>{{$novedad->titulo}}</td>
                            <td>{{$novedad->contenido}}</td>
                            <td>{{$novedad->url}}</td>
                            <td width="10px;"><a href="{{route('admin.novedades.edit', $novedad)}}" class="btn btn-primary btn-sm">Editar</a></td>
                            <td  width="10px;">
                                {!! Form::open(['route'=>['admin.novedades.destroy',$novedad], 'method'=> 'DELETE']) !!}
                                {!! Form::submit('Eliminar', ['class'=>'btn btn-danger btn-sm']) !!}
                                {!! Form::close() !!}

                            </td>

                        </tr>   
                        @empty
                        <tr>
                            <td colspan="5"> No hay registros</td>
                        </tr> 
                        @endforelse
                                              
                    </tbody>
            </table>
        </div>
    </div>
@stop

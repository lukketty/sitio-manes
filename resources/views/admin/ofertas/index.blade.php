@extends('adminlte::page')

@section('title', 'Manes Web panel')

@section('content_header')
    <h1>Ofertas</h1>
@stop

@section('content')
@if (session('info'))
    <div class="alert alert-danger">
        {{session('info')}}
    </div>
@endif
    <div class="card">
        <div class="card-header">
           <a href="{{route('admin.ofertas.create')}}" class="btn btn-info">crear Item</a> 
        </div>
        <div class="card-body">
            <table class="table table-striped ">
                <thead class="thead-inverse">
                    <tr>
                        <th>Nro </th>
                        <th>Usuario</th>
                        <th>Titulo</th>
                        <th>resumen</th>
                        <th colspan="2">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse ($oferta as $item)

                        <tr>
                            <td scope="row">{{$item->id}}</td>
                            <td>{{$item->user->name}}</td>
                            <td>{{$item->titulo}}</td>
                            <td>{{$item->descripcion}}</td>
                            <td width="10px;"><a href="{{route('admin.ofertas.edit', $item)}}" class="btn btn-primary btn-sm">Editar</a></td>
                            <td width="10px;">
                                {!! Form::open(['route'=>['admin.ofertas.destroy',$item], 'method'=> 'DELETE']) !!}
                                {!! Form::submit('Eliminar', ['class'=>'btn btn-danger btn-sm']) !!}
                                {!! Form::close() !!}

                            </td>

                        </tr>   
                        @empty
                        <tr>
                            <td colspan="5"> No hay registros</td>
                        </tr> 
                        @endforelse
                                              
                    </tbody>
            </table>
        </div>
    </div>
@stop

@extends('layouts.app')

@section('resto')

<section class="well1 ins1">
  <div class="container hr">
    <p class=titulo4>{{$blog->titulo}}</h1>
  <div class="row off2" >
    
  
    
    <div class="grid_8">
      <div class="grid_12" style="margin-bottom:20px; ">
        {!!$blog->descripcion!!}
      </div>
      <div class="grid-8" style="margin-bottom:20px; ">
        <img src="{{Storage::url($blog->imagen)}}" alt="{{$blog->descripcion}}" width="50%" style="margin-left:30px;">

      </div>
      
      
      <div class="grid_8" style="margin-bottom:50px; ">
        {!!$blog->contenido!!}
  
      </div>
      <div class="grid_12">
        <hr>
        <a href="{{route('blogs')}}" class="btn">Volver</a>
  
      </div>
    </div>
    <div class="grid-4">
      
     
      @empty(!$post)
      <h6>Otros post</h6>
          
      @endempty
      
      <table style="width: 350px;">
       
      @foreach ($post as $item)
      <tr>
        <td style="width: 30%">
          <a href="{{route('nota', $item)}}" > 
            <img src="{{Storage::url($item->imagen)}}" style="width: 80%"> 
          </a>
        </td>
        <td>
          <a href="{{route('nota', $item)}}" > 
         <strong> {{$item->titulo}}</strong>
          <p>{{Str::substr($item->descripcion, 0, 30)}}...</p>
          </a>
        </td>
      </tr>
    
      @endforeach
    
  </table>
      
     </div>
    
   
  </div>
  
   
  
  </div>
      
</section>
    
@endsection

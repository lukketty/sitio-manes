@extends('layouts.app')

@section('resto')
<section class="well1">
    <div class="container">
      <h1>Ingreso a Operadores PF </h1>
      <p>Ingrese segun su categoría al sistema</p>
      <hr>

      <div class="row off2">
        <div class="grid_4"><img src="{{url('/')}}/images/pfpc.png" alt="">
          <h3>Operador Manes</h3>
          <p>Uso interno. Ingreso para el personal PF</p><br> <a href="https://alt.manes.com.ar/sii" class="btn">Ingresar</a>
        </div>
        <div class="grid_4"><img src="{{url('/')}}/images/pfpc.png" alt="">
          <h3>Operador Obra Social</h3>
          <p>Acceso para centrales de obras sociales.<br>Administración, auditorías, carga de pacientes y carga de recetas.</p><a href="https://alt.manes.com.ar/st" class="btn">Ingresar</a>
        </div>
        <div class="grid_4"><img src="{{url('/')}}/images/pfpc.png" alt="">
          <h3>Operador Delegación</h3>
          <p>Acceso para delegaciones de obras sociales.  administracion, carga de pacientes y carga de recetas.</p><a href="https://alt.manes.com.ar/psppo" class="btn">Ingresar</a>
        </div>
      </div>
      <hr>
 
    </div>
  </section>
    
@endsection

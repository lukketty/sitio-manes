@extends('layouts.app')

@section('resto')
<section class="well1">
    <div class="container">
      <p class="titulo6">Comunicate con nosotros</p>
      <hr>
      <ul class="contact-list2 row">
        <li class="grid_4">
          <h3>Atención General</h3><a href="callto:#">221 425 0100</a>
          <dl>
            <dt>Lunes a viernes 08:00 a 21:00 hs</dt>
           
          </dl>
          <dl>
            <dt>Sábados </dt>
            <dd>8:30 a 14 hs</dd>
            <dd></dd>
          </dl>
        </li>
        <li class="grid_4">
          <h3>Pedidos Express</h3><a href="callto:#">221 422 0550</a>
          <dl><dt>Líneas rotativas</dt></dl>
          <dl><dt>Lunes a viernes 08:00 a 21:00 hs</dt>
          </dl>
          <dl>
            <dt></dt>
            <dd></dd>
            <dd></dd>
          </dl>
        </li>
        <li class="grid_4">
          <h3>Sector de Compras y proovedores</h3><a href="callto:#">221 425 0100 interno 214</a>
          <dl>
            <dt>Lunes a viernes 08:00 a 21:00 hs</dt>
           
          </dl>
          <dl>
            <dt>Sábados </dt>
            <dd>8:30 a 14 hs</dd>
            <dd></dd>
          </dl>
        </li>
        </li>
      </ul>
                  <ul class="contact-list2 row">
      
        <li class="grid_4">
          <h3>Tesorería</h3><a href="callto:#">221 425 0100 interno 107</a>
          <dl>
            <dt>Lunes a viernes</dt>
          </dl>
          <dl>
            <dt>08:00 a 16:00 hs</dt>
          </dl>
        </li>
        <li class="grid_4">
          <h3>Atención Pacientes OSPE</h3><a href="callto:#">221 425 0100 opción 2</a>
          <dl><dt>Lunes - Viernes</dt></dl>
            <dl><dd>08:00 a 17:00 hs</dd>
          </dl>
        </li>
        </li>
      </ul>
                  <ul class="contact-list2 row">
        <li class="grid_4">
          <h3>Relaciones Institucionales</h3><a href="callto:#">221 425 0100 opción 6</a>
           <dl><dt>Lunes a viernes 08:00 a 17:00 hs</dt></dl>
        </li>
        <li class="grid_4">
          <h3>Relaciones Internacionales</h3><a href="callto:#">+54 221 425 0100 130</a>
          <dl>
            <dt>Martes a viernes 08:00 a 13:00 hs</dt></dl><dl>
          </dl>
        </li>
       
        </li>
      </ul>          
      
    </div>
  </section>
  <section class="well bg-secondary2">
    <div class="container">
                  <h1>A través de los medios digitales...</h1>
      <ul class="row product-list">
        <li class="grid_6">
          <div class="box wow fadeInRight">
            <div class="box_aside">
              <div class="icon fa-facebook-official"></div>
            </div>
            <div class="box_cnt__no-flow">
              <h3><a href="#">Facebook</a></h3>
              <p>Encontrá nuestra página de facebook. Enterate de las novedades, nuevos eventos y proyectos que tenemos en la farmacia.</p>
            </div>
          </div>
          <hr>
          <div data-wow-delay="0.3s" class="box wow fadeInRight">
            <div class="box_aside">
              <div class="icon fa-envelope"></div>
            </div>
            <div class="box_cnt__no-flow">
              <h3><a href="#">Correo electrónico</a></h3>
              <p>Enviá tu consulta a institucional@manes.com.ar y será redirigida al sector indicado. Tiempo máximo estimado de respuesta: 36 hs</p>
            </div>
          </div>
        </li>
        <li class="grid_6">
          <div data-wow-delay="0.2s" class="box wow fadeInRight">
            <div class="box_aside">
              <div class="icon fa-instagram"></div>
            </div>
            <div class="box_cnt__no-flow">
              <h3><a href="#">Instagram</a></h3>
              <p>Te compartimos infografías, novedades de eventos, videos instructivos y más.</p>
            </div>
          </div>
          <hr>
          <div data-wow-delay="0.4s" class="box wow fadeInRight">
            <div class="box_aside">
              <div class="icon fa-whatsapp"></div>
            </div>
            <div class="box_cnt__no-flow">
              <h3><a href="#">WhatsApp </a></h3>
              <p>¿Tenés alguna consulta de productos? Podés comunicarte por WhatsApp al +54 9 221 598 1987 (Atención: Sólo se atienden consultas a través de la aplicación).</p>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </section>
    
@endsection

@extends('layouts.app')

@section('resto')
<section class="well1">
    <div class="container">
      <h2>Consulte su trámite Meppes - Camoyte</h2>
      <p>Ingrese su número de orden para poder consultar el estado de su trámite</p>
             
       <script src='https://www.google.com/recaptcha/api.js'></script>
       
      <form action="./respuesta" method="POST" >
         <hr>  
        <div class="grid_4">
          @csrf
             <input  type="hidden" value="1722108" name="servicio"  />
             <input class="inputform" type="text" name="nro_orden" id="nro_orden" placeholder="Nro de Orden" required />
             <div class="g-recaptcha" data-sitekey="6LdoMXIUAAAAAB9wBCjaeiY9QxuSN0cMuB8YRfwb"></div>
             <input class="boton" type="submit" value="Enviar" name="submit">
         
        </div>
        <br>
       <hr>
      </form>

      
      
 
    </div>
  </section>
    
@endsection

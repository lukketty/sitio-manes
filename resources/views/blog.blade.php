@extends('layouts.app')

@section('resto')
<section class="well1 ins1">
  <div class="container hr">
  <p class="titulo5" style="text-align: center;">BLOG DE NOVEDADES</p>
  @php
  $i=1;    
  @endphp
      @forelse ($post as $item)

        @if (($loop->first) || ($i==1))
        <div class="row off2" >
        @endif
        
        <div class="grid_4"><img src="{{Storage::url($item->imagen)}}" alt="{{$item->descripcion}}" width="100%">
          <h3>{{$item->titulo}}</h3>
          <p>{!!$item->descripcion!!}</p>
          <a href="{{route('nota', $item)}}" class="btn">Ver mas</a>
        </div>

        @if (($loop->last)||($i==3))
        </div>
        <hr>
        @php
        $i=0;    
        @endphp
        @endif
        @php
        $i++;    
        @endphp
      @empty
      <div class="row off2" style="text-align:center">
          <div class="grid_12"><img src="./images/nada-por-aca.jpg" alt="Sin posteos por el momento">
            <h3>¡Sin Posteos!</h3>
            <p>No hay post por el momento</p>
          </div>
      </div>
      @endforelse
      
</section>
    
@endsection

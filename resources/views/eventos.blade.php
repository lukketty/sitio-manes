@extends('layouts.app')

@section('resto')
<section class="well1 ins1">
  <div class="container hr">
  <p class="titulo5" style="text-align: center;">PRÓXIMOS EVENTOS</p>
    
      @forelse ($eventos as $evento)
      @if ($loop->odd)
      <div class="row off2" style="text-align: center;">
      @endif
      
      <div class="grid_6"><img src="{{Storage::url($evento->imagen)}}" alt="{{$evento->descripcion}}">
        <h3>{{$evento->titulo}}</h3>
        <p>{{$evento->contenido}}</p>
        <a href="{{$evento->url}}" class="btn">INSCRIBIRSE</a>
      </div>

      @if ($loop->even)
      </div>
      @endif
      @empty
      <div class="row off2">
          <div class="grid_6"><img src="{{url('/')}}/images/sin-eventos.jpg" alt="sin enventos por el momento">
            <h3>¡Sin eventos!</h3>
            <p>No hay eventos por el momento</p>
          </div>
      </div>
      @endforelse
      
</section>
    
@endsection

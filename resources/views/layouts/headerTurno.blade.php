<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Turnos On-line</title>
    <link rel="stylesheet" href="./css/turnos.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
   @livewireStyles
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="  crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/504cdd56fe.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    
</head>
<body >
  

    <nav class="navbar navbar-expand-sm shadow-sm p-3 mb-5 bg-white">
        
         
              <div class="container-fluid">
                
                    <div class="col-sm-3 ">
                      <img src="{{url('/')}}/images/maneslogo.png">
                    </div>
                    <div class="col-sm-6" style="text-align: center;">
                        <h1>Reserva de Turnos on-line</h1>
                    </div>
                    <div class="col-sm-3">
                      <p>Sed ut perspiciatis...</p>
                      <p> <a href="https://www.facebook.com/Fcia.Manes/"><i class="fa fa-facebook-square fa-lg" aria-hidden="true"></i></a>
                          <a href="https://instagram.com/farmaciamanes?utm_source=ig_profile_share&igshid=19j4bl3p5zsdj"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a>
                          <a href="https://www.youtube.com/channel/UCQLflj9D3VBg_BI3ii2S-dA"><i class="fa fa-youtube-play fa-lg" aria-hidden="true"></i></a>
                          <a href="https://plus.google.com/u/0/110218630788988009975"><i class="fa fa-google fa-lg" aria-hidden="true"></i></a>
                          <i class="fa fa-whatsapp fa-lg" aria-hidden="true"></i></p>
                    </div>
              </div>
                
             
     </nav>
     <div class="main">
         
         @yield('cuerpo')
     </div>
     <!-- Footer -->
<footer class="page-footer font-small bg-light">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2020 Copyright:
    <a href="https://web.manes.com.ar/"> Farmacia Manes</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
<script>
    $(function() {
        $('#datepicker').datepicker({
          onSelect: function(dateText) {
       $('#datepicker2').datepicker("setDate", $(this).datepicker("getDate"));
          },
           minDate: 1 
        
        });
      });
      
      $(function() {
        $("#datepicker2").datepicker();
      });
     
        $("#datepicker2").change(function() {

          var value=$("#datepicker2").val();

          $("#fecha").val(value);

        });

      
      
    </script>
    @livewireScripts
</body>
</html>
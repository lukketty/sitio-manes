<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Home</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="{{url('/')}}/images/favicon2.ico" type="image/x-icon">
    <link rel="stylesheet" href="{{url('/')}}/css/grid.css">
    <link rel="stylesheet" href="{{url('/')}}/css/style.css">
    <link rel="stylesheet" href="{{url('/')}}/css/camera.css">
    <link rel="stylesheet" href="{{url('/')}}/css/owl-carousel.css">
    <link rel="stylesheet" href="{{url('/')}}/css/inputform.css">
    <link rel="stylesheet" href="{{url('/')}}/css/misestilos.css">
    @livewireStyles
    <script src="{{url('/')}}/js/jquery.js"></script>
    <script src="{{url('/')}}/js/jquery-migrate-1.2.1.js"></script>

    <script src="{{url('/')}}/sliderengine/amazingslider.js"></script>
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/sliderengine/amazingslider-1.css">
    <script src="{{url('/')}}/sliderengine/initslider-1.js"></script>

    <script src="{{url('/')}}/js/html5shiv.js"></script><![endif]-->
    <script src="{{url('/')}}/js/device.min.js"></script>
      <!--Start of Zopim Live Chat Script-->
      <script type="text/javascript">
      window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
      d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
      _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
      $.src='//v2.zopim.com/?2cYWr8POnmd1UCtVTKjFr2MoFk4ZDNbG';z.t=+new Date;$.
      type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
      </script>
      <!--End of Zopim Live Chat Script-->

  </head>
    <body>
    <div class="page">

<header>
<div class="container">
<div class="brand">
<img src="{{url('/')}}/images/cabmanes.png">
<p class="brand_slogan" style="font-size: 40px; padding-bottom:20px;"> Farmacia</p>
</div>
<a href="callto:#" class="fa-phone">0221-425-0100</a>

<p class="">
<a href="https://www.facebook.com/Fcia.Manes/"><i class="fa fa-facebook-square fa-lg" aria-hidden="true"></i></a>
<a href="https://instagram.com/farmaciamanes?utm_source=ig_profile_share&igshid=19j4bl3p5zsdj"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a>
<a href="https://www.youtube.com/channel/UCQLflj9D3VBg_BI3ii2S-dA"><i class="fa fa-youtube-play fa-lg" aria-hidden="true"></i></a>
<a href="https://plus.google.com/u/0/110218630788988009975"><i class="fa fa-google fa-lg" aria-hidden="true"></i></a>
<i class="fa fa-whatsapp fa-lg" aria-hidden="true"></i>

</p>
</div>
<div id="stuck_container" class="stuck_container">
<div class="container">
<nav class="nav">
<ul data-type="navbar" class="sf-menu">
<li class="active"><a href="{{url('/')}}/">Inicio</a>
</li>
<li><a href="{{url('/')}}/quienes">Quiénes somos</a>
</li>
<li><a href="#">Consultas</a>
<ul>
<li><a href="{{url('/')}}/consultas/meppes">Meppes - Camoyte</a></li>
<li><a href="{{url('/')}}/consultas/eventos">Eventos</a></li>
<li><a href="{{url('/')}}/consultas/vacunatorio">Vacunatorio</a></li>
</ul>
</li>

<li><a href="#">PF</a>
  <ul>
  <li><a href="https://alt.manes.com.ar/pspdiab/">Pacientes PF</a></li>
  <li><a href="http://sistemas.manes.com.ar/login">Portal PF</a></li>
  <li><a href="{{url('/')}}/pfpc/">Operadores PF</a></li>
</ul>
</li>
<li><a href="#">Comerciales</a>
<ul>
<li><a href="#">Numerador Digital</a></li>
<li><a href="https://www.manes.com.ar/mview/">Publicidad y noticias</a></li>
<li><a href="https://www.manes.com.ar/malert/">Mensajería</a></li>
<li><a href="https://www.manes.com.ar/reportes/">Reportes</a></li>
</ul>
</li>

<li><a href="{{url('/')}}/contactenos">Contáctenos</a>
</li>
</ul>
</nav>
</div>
</div>
</header>
<!--
========================================================
        CONTENT
========================================================
-->

<!--
    ========================================================
                                CONTENT
    ========================================================
    -->
    <main>
      @yield('banner')
      @yield('eventos')
      @yield('ofertas')
      @yield('publicidad')
      @yield('resto')


    </main>
    <!--
        ========================================================
                                    FOOTER
        ========================================================
        -->
        <footer>
          <section class="well4">
            <div class="container">

                <div class="grid_3">
                   <div class="contact-list2 row">
                      <h3>Información general</h3>
                        <ul>
                          <li><a href="{{url('/')}}/quienes">Quiénes somos</a></li>
                          <li>Preguntas Frecuentes</li>
                          <li><a href="{{url('/')}}/contactenos">Contáctenos</a></li>
                        </ul>
                  </div>
                </div>

                <div class="grid_3">
                   <div class="contact-list2 row">
                     <h3> Consultas </h3>
                        <ul >
                          <li><a href="https://www.manes.com.ar/pspdiab/index.php">Historia clínica electrónica Progama Farmacéutico</a></li>
                          <li><a href="http://sistemas.manes.com.ar/login">Portal paciente Progama Farmacéutico</a></li>
                          <li><a href="{{url('/')}}/consultas/meppes">Trámite Meppes-Camoyte</a></li>
                          <li><a href="#">Tienda on-line</a></li>
                          <li><a href="#">Turno Consultorio Progama Farmacéutico</a></li>
                          <li><a href="{{url('/')}}/consultas/vacunatorio">Vacunatorio</a></li>
                        </ul>
                  </div>
                </div>
                <div class="grid_3">
                   <div class="contact-list2 row">
                      <h3>Servicios</h3>
                        <ul>
                          <li><a href="https://www.manes.com.ar/malert/">Mensajería</a></li>
                          <li><a href="https://www.manes.com.ar/mview/">Publicidad</a></li>
                          <li><a href="#">Numerador</a></li>
                        </ul>
                  </div>
                </div>

                <div>
                  <div class="contact-list2 row">
                    <h3>Social</h3>
                    <ul>
                        <li><a href="https://www.facebook.com/Fcia.Manes"><i class="fa fa-facebook-square fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Facebook</a></li>
                        <li><a href="https://www.instagram.com/farmaciamanes"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Instagram</a></li>
                        <li><a href="{{url('/')}}/consultas/eventos"><i class="fa fa-calendar fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Eventos</a></li>
                    </ul>
                  </div>
                </div>
            </div>
          </section>
          <section>
            <div class="container">

              <div class="copyright"><a href="https://qr.afip.gob.ar/?qr=Yit1Gll_hWWkqpnBUg6VMw,," target="_F960AFIPInfo"><img src="https://www.afip.gob.ar/images/f960/DATAWEB.jpg" border="0" width="30px" height="30px"></a> Farmacia Manes © <span id="copyright-year">2018</span>. &nbsp;&nbsp;
                <a href="{{url('/')}}/ppriv">Política de Privacidad Ferrere SCS Farmacia Manes</a>
              </div>
            </div>
          </section>
        </footer>
      </div>
      <script src="{{url('/')}}/js/script.js"></script>
      @livewireScripts
    </body>
  </html>

@extends('layouts.headerTurno')
@section('cuerpo')
    

<div class="container">
    <div class="row">
           <div class="col-lg-12">
               <div class="card mb-3">
               
                   <div class="card-body">
                       <h4 class="card-title">Reserve su turno on-line</h4>
                       <p class="card-text">Puede solicitar un turno para cualquier dia habil de la semana con un minimo de 24 hs de registro.</p>
                       <p class="card-text">Proporcione un mail al que tenga acceso ya que debe confirmar el turno</p>
                   </div>
               </div>             
           </div>
       </div>
           <div class="row">
           <div class="col-lg-4">
             <div class="card mb-3">
             
                 <div class="card-body">
                     <h4 class="card-title">Seleccione la fecha</h4>
                     <p>Recuerde que el turno solo se puede pedir con un minimo de un dia de anticipacion.
                         por eso el calendario comienza a partir del dia de mañana.
                     </p>
                     <div id="datepicker"></div>

                    <p>Date: <input type="text" id="datepicker2"  wire:model="fecha"></p>

                    
                     
            
                     
                     
                 </div>
             </div>                 
         </div>
         <div class="col-lg-4">
           <div class="card mb-3">
           
               <div class="card-body">
                   <h4 class="card-title">Seleccione la hora</h4>
                   <p> Solo puede seleccionar las horas disponibles que quedan del dia.</p>
                   <p>los turnos se fraccionan en 15 min.</p>
                   
                         @livewire('show-turnos')                   
                   
               </div>
           </div>                 
       </div>
       <div class="col-lg-4">
         <div class="card mb-3">
         
             <div class="card-body">
                 <h4 class="card-title">Complete sus datos</h4>
                 
                 <p class="card-text">Proporcione un Email al que tenga acceso ya que debe confirmar el turno</p>
                 {!! Form::label('nombre', 'Nombre') !!}
                 {!! Form::text('nombre', null , ['class'=>'form-control', 'id'=>'nombre']) !!}
                 {!! Form::label('apellido', 'Apellido') !!}
                 {!! Form::text('apellido', null , ['class'=>'form-control', 'id'=>'apellido']) !!}
                 {!! Form::label('dni', 'DNI') !!}
                 {!! Form::text('dni', null , ['class'=>'form-control', 'id'=>'dni']) !!}
                 {!! Form::label('email', 'Email') !!}
                 {!! Form::text('email', null , ['class'=>'form-control', 'id'=>'email']) !!}
             </div>
         </div>                 
     </div>
   </div>
   <div class="row" style="text-align: center;">
     <div class="col-lg-12">
         <div class="card mb-3">
         
             <div class="card-body">
               
                 <button class="btn btn-success" type="submit">Confirmar turno!</button>
                 
             </div>
         </div>             
     </div>
 </div>
  </div>
  @endsection
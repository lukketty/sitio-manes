<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofertas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
           $table->foreign('user_id')->references('id')->on('users');
            $table->string('titulo', 200)->nullable()->default('');
            $table->string('imagen', 200)->nullable()->default('');
            $table->text('descripcion')->nullable();
            $table->text('contenido')->nullable();
            $table->boolean('activo')->nullable()->default(true);
            $table->timestamps();
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofertas');
    }
}

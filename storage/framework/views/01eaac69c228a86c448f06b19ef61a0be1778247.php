<?php $__env->startSection('banner'); ?>
<section class="camera_container">
    <div id="camera" class="camera_wrap">
     <div data-src="./images/front_1.jpg">
        <div class="camera_caption fadeIn">
          <div class="container">
            <div class="row">
              <div class="preffix_6 grid_6" style="margin-top: 150px"><p class="titulo1" >PF:</p>
                                             <p class="titulo2" >Renovando la atención farmacéutica</p>
                                             <p class="titulo3" >Una nueva perspectiva en el ámbito farmaceútico</p> 
                                             <br><div align="center"><a href="<?php echo e(url('/')); ?>/site/quienes/redpfpc" class="titulo13"><b>¿Querés sumarte a la red PF? <br> Laboratorios & Farmacias. Clic aquí </b></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div data-src="./images/front_2.jpg">
        <div class="camera_caption fadeIn">
          <div class="container">
            <div class="row">
              <div class="preffix_6 grid_6" style="margin-top: 150px"><p class="titulo1" >Red farmaceútica:</p>
                                             <p class="titulo2" >Droguería Manes</p>
                                             <p class="titulo3" >Servicio a farmacias en toda la provincia</p> 
                                             <br><br><br><br><div align="center"><a href="<?php echo e(url('/')); ?>/site/quienes/redpfpc" class="titulo13"><b>¿Querés sumarte a la red PF? <br> Laboratorios & Farmacias. Clic aquí </b></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div data-src="./images/front_3.jpg">
        <div class="camera_caption fadeIn">
          <div class="container">
            <div class="row">
              <div class="preffix_6 grid_6" style="margin-top: 150px"> <p class="titulo1" >Servicios integrales:</p>
                                             <p class="titulo2" >Pacientes PF</p>
                                             <p class="titulo3" >Historia clínica on-line y más</p>
                                             <br><br><br><div align="center"><a href="<?php echo e(url('/')); ?>/site/quienes/redpfpc" class="titulo13"><b>¿Querés sumarte a la red PF? <br> Laboratorios & Farmacias. Clic aquí </b></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div data-src="./images/front_4.jpg">
        <div class="camera_caption fadeIn">
          <div class="container">
            <div class="row">
              <div class="preffix_6 grid_6" style="margin-top: 150px"> <p class="titulo1" >Cobertura:</p>
                                             <p class="titulo2" >Más de 100 Obras sociales, planes y coseguros</p>
                                             <br><br><br><div align="center"><a href="<?php echo e(url('/')); ?>/site/quienes/redpfpc" class="titulo13"><b>¿Querés sumarte a la red PF? <br> Laboratorios & Farmacias. Clic aquí </b></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div data-src="./images/front_5.jpg">
        <div class="camera_caption fadeIn">
          <div class="container">
            <div class="row">
              <div class="preffix_6 grid_6" style="margin-top: 150px"> <p class="titulo1" >PF:</p>
                                             <p class="titulo2" >Consultorio Farmacéutico</p>
                                             <p class="titulo3" >Atención personalizada y priorizada</p>
                                             <br><br><br><div align="center"><a href="<?php echo e(url('/')); ?>/site/quienes/redpfpc" class="titulo13"><b>¿Querés sumarte a la red PF? <br> Laboratorios & Farmacias. Clic aquí </b></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
       <div data-src="./images/front_6.jpg">
        <div class="camera_caption fadeIn">
          <div class="container">
            <div class="row">
              <div class="preffix_6 grid_6" style="margin-top: 150px"> <p class="titulo1" >Próximamente:</p>
                                             <p class="titulo2" >Manes tienda On-line</p>
                                             <p class="titulo3" >Comprá, pagá con tarjeta, retirá.</p>
                                             <br><br><br><div align="center"><a href="<?php echo e(url('/')); ?>/site/quienes/redpfpc" class="titulo13"><b>¿Querés sumarte a la red PF? <br> Laboratorios & Farmacias. Clic aquí </b></a></div> 
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </section>
  <section>
    <!-- WhatsHelp.io widget -->
<script type="text/javascript">
(function () {
  var options = {
      whatsapp: "+542215981987", // WhatsApp number
      call_to_action: "¿Interesado en el PF?", // Call to action
      position: "left", // Position may be 'right' or 'left'
  };
  var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
  var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
  s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
  var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
})();
</script>
<!-- /WhatsHelp.io widget -->

    <div class="container banner_wr">
      
      <ul class="banner">
        <li style="cursor: pointer;" onclick="window.location='https://farmaciamanes.mitiendanube.com/';">
          <img src="<?php echo e(url('/')); ?>/image/icono-tienda.png" alt="" style="width: 40%">
           <h3>Tienda On-line</h3>
           
         </li>
        <li style="cursor: pointer;" onclick="window.location='<?php echo e(url('/')); ?>/si';">
          <div class="fa fa-calendar" ></div>
          <h3>Drogueria</h3> 
          
        </li>
       <!-- 
        <li style="cursor: pointer;" onclick="window.location='<?php echo e(url('/')); ?>/turnos';">
          <div class="fa fa-calendar" ></div>
          <h3>Turno on-line</h3> 
          
        </li>
        --!>
       
        <li style="cursor: pointer;" onclick="window.location='<?php echo e(url('/')); ?>/blog';">
          <div class="fa fa-newspaper-o"></div>
          <h3>Blogs de Novedades</h3>
          
        </li>
        <li style="cursor: pointer;" onclick="window.location='<?php echo e(url('/')); ?>/pspdiab';">
          <div class="fa fa-bar-chart"></div>
          <h3>Pacientes PF</h3>
        </li>
        <li style="cursor: pointer;" onclick="window.location='./consultas/meppes';">
          <div class="fa fa-file-text"></div>
          <h3>Planes Especiales<br/>Meppes - Camoyte</h3>
        </li>
      </ul>
    </div>
  </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('ofertas'); ?>
<section class="well1 ins1">
  <div class="container hr">
    <p class="titulo4" style="text-align: center;">Ofertas del mes</p>
      <ul class="row product-list">  
       
      <?php $__empty_1 = true; $__currentLoopData = $ofertas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $oferta): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
         <li class="grid_6" style="margin-top: 20px; background: #badaf5; ">
        <div class="box wow fadeInRight">
          <div class="box_aside">
            <div class="iconNew"> <img src="<?php echo e(Storage::url($oferta->imagen)); ?>" alt="<?php echo e($oferta->descripcion); ?>" style="width: 100%"></div>
          </div>
          <div class="box_cnt__no-flow">
            <h3><?php echo e($oferta->titulo); ?></h3>
            <p><?php echo $oferta->contenido; ?></p>
          </div>
        </div>
        
      </li>
      
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
      <div class="row off2" style="text-align: center;">
          <div class="grid_12"><img src="<?php echo e(url('/')); ?>/images/sin-ofertas.png" alt="sin enventos por el momento"  width="30%" style="padding-left: 80px;" >
                      
          </div>
      </div>
      <?php endif; ?>
      </ul>
  </div>
      
</section>

    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('eventos'); ?>
<section class="well1 ins1">
    <div class="container hr">
    <p class="titulo5" style="text-align: center;">PRÓXIMOS EVENTOS</p>
      
        <?php $__empty_1 = true; $__currentLoopData = $eventos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $evento): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        <?php if($loop->odd): ?>
        <div class="row off2" style="text-align: center;">
        <?php endif; ?>
        
        <div class="grid_6"><img src="<?php echo e(Storage::url($evento->imagen)); ?>" alt="<?php echo e($evento->descripcion); ?>">
          <h3><?php echo e($evento->titulo); ?></h3>
          <p><?php echo e($evento->contenido); ?></p>
          <a href="<?php echo e($evento->url); ?>" class="btn">INSCRIBIRSE</a>
        </div>

        <?php if($loop->even): ?>
        </div>
        <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <div class="row off2" style="text-align: center;">
            <div class="grid_12"><img src="<?php echo e(url('/')); ?>/images/sin-eventos.jpg" alt="sin enventos por el momento" style="width: 40%">
             
              <p>No hay eventos proximos </p>
            </div>
        </div>
        <?php endif; ?>
    </div>
        
  </section>
 
  
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('publicidad'); ?>

<section class="well1 ins1">
    <div class="container hr">
      <div class="row">
        <div class="grid_12">
            <div id="amazingslider-wrapper-1" style="display:block;position:relative;max-width:750px;margin:0px auto 0px;">
                  <div id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
                      <ul class="amazingslider-slides" style="display:none;">
                        
                       <?php $__empty_1 = true; $__currentLoopData = $fila; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                       <?php if($item->tipo=="vid"): ?>
                       <li><video preload="none" src="<?php echo e($urlPubli); ?>images/<?php echo e($item->url); ?>" ></video></li>
                             
                       <?php else: ?>
                           
                       <li><img src="<?php echo e($urlPubli); ?>images/<?php echo e($item->url); ?>" alt="<?php echo e($item->alt); ?>" data-description="<?php echo e($item->alt); ?>" title="<?php echo e($item->nombre); ?>" /></li>
                       <?php endif; ?>
                      
                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                          <h1> No hay nada</h1>
                       <?php endif; ?>
                        
                         
                      </ul>
                     
                  <div class="amazingslider-engine"><a href="http://amazingslider.com" title="Responsive JavaScript Image Slideshow">Responsive JavaScript Image Slideshow</a></div>
                  </div>
              </div>
        </div>              
    </div>
    </div>
  </section>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('resto'); ?>
<section class="well">
    <div class="container">
      <ul class="row product-list">
        <li class="grid_6">
          <div class="box wow fadeInRight">
            <div class="box_aside">
              <div class="icon fa-envelope-o"></div>
            </div>
            <div class="box_cnt__no-flow">
              <h3><a href="<?php echo e(url('/')); ?>/pfpc/formulario_curriculum.php">¿Querés trabajar con nosotros?</a></h3>
              <p>A través de la página podrás enviarnos tu CV y estar dentro de nuestra lista de solicitudes para ser considerado.</p>
            </div>
          </div>
          <hr>
          <div data-wow-delay="0.2s" class="box wow fadeInRight">
            <div class="box_aside">
              <div class="icon fa-check-circle-o"></div>
            </div>
            <div class="box_cnt__no-flow">
              <h3><a href="https://docs.google.com/forms/d/e/1FAIpQLSd2EptZL15lZsbXO1z_RUlPOIi4_GVm-Cp4wZ3gQjAQGZMSBA/viewform?usp=sf_link">Calificá nuestra atención</a></h3>
              <p>Ayudanos a crecer y mejorar nuestra atención para brindarte un mejor servicio. Calificá nuestra atención en farmacia, en nuestro chat, atención telefónica, etc.</p>
            </div>
          </div>
        </li>
        <li class="grid_6">
          <div data-wow-delay="0.3s" class="box wow fadeInRight">
            <div class="box_aside">
              <div class="icon fa-calendar"></div>
            </div>
            <div class="box_cnt__no-flow">
              <h3><a href="http://www.colfarmalp.org.ar/turnos-la-plata/">Farmacia de turno</a></h3>
              <p>¿Necesitás saber qué farmacias se encuentran de turno hoy? Te proveemos la lista de farmacias de La Plata que se encuentran de turnos en el mes.</p>
            </div>
          </div>
          <hr>
          <div data-wow-delay="0.4s" class="box wow fadeInRight">
            <div class="box_aside">
              <div class="icon fa-book"></div>
            </div>
            <div class="box_cnt__no-flow">
              <h3><a href="<?php echo e(url('/')); ?>/recertarios/samples/basic/menu.php">Recetarios on-line </a></h3>
              <p>Te acercamos recetas de comida saludable para que descubras la posibilidad de comer rico y sano a la vez. Recetas para diabéticos, celíacos, hipertensos y próximamente muchas más. </p>
            </div>
          </div>
        </li>
      </ul>
      
    </div>
  </section >
    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/sitio/resources/views/inicio.blade.php ENDPATH**/ ?>
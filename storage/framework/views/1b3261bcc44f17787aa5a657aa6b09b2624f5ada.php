<?php $__env->startSection('resto'); ?>

<section class="well1 ins1">
  <div class="container hr">
    <p class=titulo4><?php echo e($blog->titulo); ?></h1>
  <div class="row off2" >
    
  
    
    <div class="grid_8">
      <div class="grid_12" style="margin-bottom:20px; ">
        <?php echo $blog->descripcion; ?>

      </div>
      <div class="grid-8" style="margin-bottom:20px; ">
        <img src="<?php echo e(Storage::url($blog->imagen)); ?>" alt="<?php echo e($blog->descripcion); ?>" width="50%" style="margin-left:30px;">

      </div>
      
      
      <div class="grid_8" style="margin-bottom:50px; ">
        <?php echo $blog->contenido; ?>

  
      </div>
      <div class="grid_12">
        <hr>
        <a href="<?php echo e(route('blogs')); ?>" class="btn">Volver</a>
  
      </div>
    </div>
    <div class="grid-4">
      
     
      <?php if(empty(!$post)): ?>
      <h6>Otros post</h6>
          
      <?php endif; ?>
      
      <table style="width: 350px;">
       
      <?php $__currentLoopData = $post; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <td style="width: 30%">
          <a href="<?php echo e(route('nota', $item)); ?>" > 
            <img src="<?php echo e(Storage::url($item->imagen)); ?>" style="width: 80%"> 
          </a>
        </td>
        <td>
          <a href="<?php echo e(route('nota', $item)); ?>" > 
         <strong> <?php echo e($item->titulo); ?></strong>
          <p><?php echo e(Str::substr($item->descripcion, 0, 30)); ?>...</p>
          </a>
        </td>
      </tr>
    
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    
  </table>
      
     </div>
    
   
  </div>
  
   
  
  </div>
      
</section>
    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/sitio/resources/views/nota.blade.php ENDPATH**/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Turnos On-line</title>
    <link rel="stylesheet" href="./css/turnos.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/504cdd56fe.js" crossorigin="anonymous"></script>
</head>
<body >
  

    <nav class="navbar navbar-expand-sm shadow-sm p-3 mb-5 bg-white">
        
         
              <div class="container-fluid">
                
                    <div class="col-sm-3 ">
                      <img src="<?php echo e(url('/')); ?>/images/maneslogo.png">
                    </div>
                    <div class="col-sm-6" style="text-align: center;">
                        <h1>Reserva de Turnos on-line</h1>
                    </div>
                    <div class="col-sm-3">
                      <p>Sed ut perspiciatis...</p>
                      <p> <a href="https://www.facebook.com/Fcia.Manes/"><i class="fa fa-facebook-square fa-lg" aria-hidden="true"></i></a>
                          <a href="https://instagram.com/farmaciamanes?utm_source=ig_profile_share&igshid=19j4bl3p5zsdj"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a>
                          <a href="https://www.youtube.com/channel/UCQLflj9D3VBg_BI3ii2S-dA"><i class="fa fa-youtube-play fa-lg" aria-hidden="true"></i></a>
                          <a href="https://plus.google.com/u/0/110218630788988009975"><i class="fa fa-google fa-lg" aria-hidden="true"></i></a>
                          <i class="fa fa-whatsapp fa-lg" aria-hidden="true"></i></p>
                    </div>
              </div>
                
             
     </nav>
     <div class="main">
         <div class="container">
           <div class="row">
                  <div class="col-lg-12">
                      <div class="card mb-3">
                      
                          <div class="card-body">
                              <h4 class="card-title">Reserve su turno on-line</h4>
                              <p class="card-text">Puede solicitar un turno para cualquier dia habil de la semana con un minimo de 24 hs de registro.</p>
                              <p class="card-text">Proporcione un mail al que tenga acceso ya que debe confirmar el turno</p>
                          </div>
                      </div>             
                  </div>
              </div>
                  <div class="row">
                  <div class="col-lg-4">
                    <div class="card mb-3">
                    
                        <div class="card-body">
                            <h4 class="card-title">Seleccione la fecha</h4>
                            <p class="card-text"><?php echo Form::date('name', \Carbon\Carbon::now());; ?></p>
                            
                        </div>
                    </div>                 
                </div>
                <div class="col-lg-4">
                  <div class="card mb-3">
                  
                      <div class="card-body">
                          <h4 class="card-title">Seleccione la hora</h4>
                          <p class="card-text">Puede solicitar un turno para cualquier dia habil de la semana con un minimo de 24 hs de registro.</p>
                          <p class="card-text">Proporcione un mail al que tenga acceso ya que debe confirmar el turno</p>
                      </div>
                  </div>                 
              </div>
              <div class="col-lg-4">
                <div class="card mb-3">
                
                    <div class="card-body">
                        <h4 class="card-title">Complete sus datos</h4>
                        <p class="card-text">Puede solicitar un turno para cualquier dia habil de la semana con un minimo de 24 hs de registro.</p>
                        <p class="card-text">Proporcione un mail al que tenga acceso ya que debe confirmar el turno</p>
                    </div>
                </div>                 
            </div>
          </div>
          <div class="row" style="text-align: center;">
            <div class="col-lg-12">
                <div class="card mb-3">
                
                    <div class="card-body">
                      
                        <button class="btn btn-success" type="submit">Confirmar turno!</button>
                        
                    </div>
                </div>             
            </div>
        </div>
         </div>
         
     </div>
     <!-- Footer -->
<footer class="page-footer font-small bg-light">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2020 Copyright:
    <a href="https://web.manes.com.ar/"> Farmacia Manes</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->

</body>
</html><?php /**PATH /var/www/sitio/resources/views/turnos.blade.php ENDPATH**/ ?>
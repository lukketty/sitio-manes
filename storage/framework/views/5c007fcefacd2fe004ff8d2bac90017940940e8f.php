<?php $__env->startSection('title', 'Manes Web panel'); ?>

<?php $__env->startSection('content_header'); ?>
    <h1>Blog</h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php if(session('info')): ?>
    <div class="alert alert-danger">
        <?php echo e(session('info')); ?>

    </div>
<?php endif; ?>
    <div class="card">
        <div class="card-header">
           <a href="<?php echo e(route('admin.blog.create')); ?>" class="btn btn-info">crear Item</a> 
        </div>
        <div class="card-body">
            <table class="table table-striped ">
                <thead class="thead-inverse">
                    <tr>
                        <th>Nro </th>
                        <th>Usuario </th>
                        <th>Titulo</th>
                        <th>resumen</th>
                        <th colspan="2">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $__empty_1 = true; $__currentLoopData = $post; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>

                        <tr>
                            <td scope="row"><?php echo e($item->id); ?></td>
                            <td><?php echo e($item->user->name); ?></td>
                            <td><?php echo e($item->titulo); ?></td>
                            <td><?php echo e($item->descripcion); ?></td>
                            <td width="10px;"><a href="<?php echo e(route('admin.blog.edit', $item)); ?>" class="btn btn-primary btn-sm">Editar</a></td>
                            <td width="10px;">
                                <?php echo Form::open(['route'=>['admin.blog.destroy',$item], 'method'=> 'DELETE']); ?>

                                <?php echo Form::submit('Eliminar', ['class'=>'btn btn-danger btn-sm']); ?>

                                <?php echo Form::close(); ?>


                            </td>

                        </tr>   
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <tr>
                            <td colspan="5"> No hay registros</td>
                        </tr> 
                        <?php endif; ?>
                                              
                    </tbody>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('adminlte::page', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/sitio/resources/views/admin/blog/index.blade.php ENDPATH**/ ?>
<?php $__env->startSection('resto'); ?>
<section class="well1 ins2 mobile-center">
    <div class="container">
      <h2>Quienes Somos</h2>
      <div class="row off2">
        <div class="grid_6 ">
          <h3>EMPRESA</h3>
          <p style="text-indent: 1em; margin-bottom: 6px">Farmacia y Droguería MANES fue creada, en 1926. Con más de 40 años en el mercado farmacéutico, Manes ha logrado convertirse en unas de las empresas más conocidas de la ciudad y ha ganado reconocimiento en varias localidades de la provincia y el país.</p>
          <p style="text-indent: 1em; margin-bottom: 6px">Nuestra experiencia, en conjunto con relaciones comerciales estratégicas con laboratorios, droguerías y obras sociales nos permite comprometernos a satisfacer las necesidades de quienes se acercan en búsqueda de no sólo insumos sino atención de calidad.</p>
          <p style="text-indent: 1em; margin-bottom: 6px">Contamos con un stock activo de medicación de alto costo, tecnologías médicas y productos médicos, así como con las herramientas para programar su entrega en tiempo y forma.</p>
          <p style="text-indent: 1em; margin-bottom: 6px">En 2015, lanzamos el Programa PF (Programa Farmacéutico para Patologías Crónicas) el cual busca darle servicios de primera calidad a los pacientes que padecen de patologías crónicas y acompañarlos en su día a día para mejorar su calidad de vida.</p>
        </div>
        <div class="grid_6 ">
          <h3>RED DE DISTRIBUCIÓN</h3>
          <p style="text-indent: 1em; margin-bottom: 6px">Droguería MANES provee a una amplia red de farmacias, obras sociales y establecimientos asistenciales de salud en cualquier lugar del país. La logística utilizada asegura la distribución sin costos adicionales para el cliente, con plazos de entrega prestablecidos y garantía de integridad de los productos.</p>
          <p style="text-indent: 1em; margin-bottom: 6px">Las consultas y pedidos son recepcionados por personal especializado a través de una serie de líneas telefónicas distribuidas en dos conmutadores, direcciones de correo electrónico y el sitio web MANES. </p>
          <p style="text-indent: 1em; margin-bottom: 6px">La celebración de convenios específicos, ajustados en cada caso a las diferentes realidades comerciales, permite modos de pago no tradicionales. La cesión de crédito futuro, entre otros, es utilizada desde hace años por más de un centenar de clientes.</p>
          <p style="text-indent: 1em; margin-bottom: 6px">La confección de alianzas estratégicas entre MANES y otros agentes de la comercialización e industria farmacéuticas, abre la posibilidad del usufructo de esta red de distribución por parte de terceros.</p>
        </div>
      </div>
      <hr>
     <div class="row">
        <div class="grid_6">
          <h3>PROGRAMA PF</h3>
          <p style="text-indent: 1em; margin-bottom: 6px"> Contamos con un Programa Farmacéutico para patologias crónicas (PF) el cual brinda atención farmacéutica personalizada a aquellas personas las padecen. Consta de servicios que tienen el fin de asistir a los pacientes y mejorar su calidad de vida.</p>
          <ul class="marked-list">
              <li>Calendario farmacéutico</li>
              <li>Entrega programada de medicación</li>
              <li>Vías de comunicación privadas, ágiles y flexibles</li>
              <li>Atención prioritaria en mostrador</li>
              <li>Entrenamiento, entrega y asistencia en tecnología médica</li>
              <li>Aviso de falta de medicación</li>
          </ul>
        </div>
        <div class="grid_6">
          <h3>PATOLOGÍAS FRECUENTES</h3>
          <p style="text-indent: 1em; margin-bottom: 6px"> Nuestro programa está destinado a pacientes que padecen diversas patologías crónicas, entre las cuales se encuentran:<br>&nbsp;</p>
          <ul class="marked-list">
              <li>Diabetes</li>
              <li>Colitis Ulcerosa & Enfermedad de Chron</li>
              <li>Fibrosis quística</li>
              <li>Esquizofrenia</li>
              <li>Vejiga neurogénica</li>
              <li>Esclerosis múltiple</li>
          </ul>
        </div>
    
      </div>
    </div>
  </section>
  <section class="well1 ins4 bg-image">
    <div class="container">
      <div class="row">
        <div class="grid_7 preffix_5">
          <h2>Necesidades específicas</h2>
          <div class="row off4">
            <div class="grid_6">
              <ul class="marked-list wow fadeInRight">
                <li><p style="text-indent: 1em; margin-bottom: 6px"> El alcance municipal, provincial y nacional del Depto. de Licitaciones de Manes es extendido y potenciado por las divisiones de importación y exportación de la empresa. De esta manera se logra la disponibilidad de prácticamente cualquier producto en existencia a precios competitivos.</p></li>
              </ul>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </section>
    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/sitio/resources/views/quienes.blade.php ENDPATH**/ ?>
<?php $__env->startSection('resto'); ?>
<section class="well1 ins1">
  <div class="container hr">
  <p class="titulo5" style="text-align: center;">PRÓXIMOS EVENTOS</p>
    
      <?php $__empty_1 = true; $__currentLoopData = $eventos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $evento): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
      <?php if($loop->odd): ?>
      <div class="row off2" style="text-align: center;">
      <?php endif; ?>
      
      <div class="grid_6"><img src="<?php echo e(Storage::url($evento->imagen)); ?>" alt="<?php echo e($evento->descripcion); ?>">
        <h3><?php echo e($evento->titulo); ?></h3>
        <p><?php echo e($evento->contenido); ?></p>
        <a href="<?php echo e($evento->url); ?>" class="btn">INSCRIBIRSE</a>
      </div>

      <?php if($loop->even): ?>
      </div>
      <?php endif; ?>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
      <div class="row off2">
          <div class="grid_6"><img src="<?php echo e($base); ?>images/sin-eventos.jpg" alt="sin enventos por el momento">
            <h3>¡Sin eventos!</h3>
            <p>No hay eventos por el momento</p>
          </div>
      </div>
      <?php endif; ?>
      
</section>
    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/sitio/resources/views/eventos.blade.php ENDPATH**/ ?>
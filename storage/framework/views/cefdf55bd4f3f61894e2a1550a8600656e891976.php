<?php $__env->startSection('title', 'Manes Web panel'); ?>


<?php $__env->startSection('content_header'); ?>
    <h1>Crear Posteo Blog</h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="card">
        <div class="card-body">
            <?php echo Form::open(['route'=>'admin.blog.store', 'autocomplete'=>'off', 'files'=>true]); ?>

            <?php echo Form::hidden('user_id', auth()->user()->id); ?>

            <div class="form-group">
                <?php echo Form::label('titulo', 'Título'); ?>

                <?php echo Form::text('titulo', null, ['class'=>'form-control', 'placeholder'=>'Ingrese el titulo del Evento']); ?>

                <?php $__errorArgs = ['titulo'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <span class="text-danger"><?php echo e($message); ?></span>
                    
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <div class="imagen-wrapper">
                        <img id="picture"  src="<?php echo e(url('/')); ?>/images/sin-imagen.jpg" alt="sin imagen">                    
                    </div>
                </div>
                <div class="col mb-3">
                    <div class="form-group">
                        <?php echo Form::label('imagen', 'Imagen'); ?>

                        <?php echo Form::file('imagen', ['class'=>'form-inline']); ?>

                        <?php $__errorArgs = ['imagen'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <span class="text-danger"><?php echo e($message); ?></span>
                            
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <p class="mt-3">
                            Seleccione la Imagen a cargar para el posteo, receuerde que la imagen debe tener un formato de dimensiones especifica, no carge imagenes muy pesadas ni muy grandes, el tamaño para un posteo y una visualizacion adecueada debe ser de 400x500 y no superar los 2MB
                        </p>
                    </div>
                   
                </div>
                
            </div>
            <div class="form-group">
                <?php echo Form::label('descripcion', 'Descripcion'); ?>

                <?php echo Form::textarea('descripcion', null, ['class'=>'form-control']); ?>

                <?php $__errorArgs = ['descripcion'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <span class="text-danger"><?php echo e($message); ?></span>
                    
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
            <div class="form-group">
                <?php echo Form::label('contenido', 'Contenido'); ?>

                <?php echo Form::textarea('contenido', null, ['class'=>'form-control']); ?>

                <?php $__errorArgs = ['contenido'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <span class="text-danger"><?php echo e($message); ?></span>
                    
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
           
           
            <?php echo Form::submit('Crear Post', ['class'=>'btn btn-primary']); ?>

            <?php echo Form::close(); ?>


        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<style>
    .imagen-wrapper{
        position: relative;
        padding-bottom: 56%;
    }
    .imagen-wrapper img{
        position: absolute;
        object-fit: cover;
        width: 100%;
        height: 100%
    }
</style>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="https://cdn.ckeditor.com/ckeditor5/26.0.0/classic/ckeditor.js"></script>
    
    <script>
        document.getElementById("imagen").addEventListener('change', cambiarImagen);

        function cambiarImagen(event){
            var file = event.target.files[0];

            var reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("picture").setAttribute('src', event.target.result);
            };

            reader.readAsDataURL(file);
        }
    </script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#contenido' ) )
            .catch( error => {
                console.error( error );
            } );
            
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::page', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/sitio/resources/views/admin/blog/create.blade.php ENDPATH**/ ?>
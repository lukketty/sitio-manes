<?php $__env->startSection('title', 'Manes Web panel'); ?>

<?php $__env->startSection('content_header'); ?>
    <h1>Eventos</h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php if(session('info')): ?>
    <div class="alert alert-danger">
        <?php echo e(session('info')); ?>

    </div>
<?php endif; ?>
    <div class="card">
        <div class="card-header">
           <a href="<?php echo e(route('admin.novedades.create')); ?>" class="btn btn-info">crear Item</a> 
        </div>
        <div class="card-body">
            <table class="table table-striped ">
                <thead class="thead-inverse">
                    <tr>
                        <th> Nro </th>
                        <th> usuario </th>
                        <th>Titulo</th>
                        <th>Contenido</th>
                        <th>Url Google</th>
                        <th colspan="2">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $__empty_1 = true; $__currentLoopData = $novedades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $novedad): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>

                        <tr>
                            <td scope="row"><?php echo e($novedad->id); ?></td>
                            <td><?php echo e($novedad->user->name); ?></td>
                            <td><?php echo e($novedad->titulo); ?></td>
                            <td><?php echo e($novedad->contenido); ?></td>
                            <td><?php echo e($novedad->url); ?></td>
                            <td width="10px;"><a href="<?php echo e(route('admin.novedades.edit', $novedad)); ?>" class="btn btn-primary btn-sm">Editar</a></td>
                            <td  width="10px;">
                                <?php echo Form::open(['route'=>['admin.novedades.destroy',$novedad], 'method'=> 'DELETE']); ?>

                                <?php echo Form::submit('Eliminar', ['class'=>'btn btn-danger btn-sm']); ?>

                                <?php echo Form::close(); ?>


                            </td>

                        </tr>   
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <tr>
                            <td colspan="5"> No hay registros</td>
                        </tr> 
                        <?php endif; ?>
                                              
                    </tbody>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('adminlte::page', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/sitio/resources/views/admin/novedades/index.blade.php ENDPATH**/ ?>
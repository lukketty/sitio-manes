<?php $__env->startSection('resto'); ?>
<section class="well1 ins1">
  <div class="container hr">
  <p class="titulo5" style="text-align: center;">BLOG DE NOVEDADES</p>
  <?php
  $i=1;    
  ?>
      <?php $__empty_1 = true; $__currentLoopData = $post; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>

        <?php if(($loop->first) || ($i==1)): ?>
        <div class="row off2" >
        <?php endif; ?>
        
        <div class="grid_4"><img src="<?php echo e(Storage::url($item->imagen)); ?>" alt="<?php echo e($item->descripcion); ?>" width="100%">
          <h3><?php echo e($item->titulo); ?></h3>
          <p><?php echo $item->descripcion; ?></p>
          <a href="<?php echo e(route('nota', $item)); ?>" class="btn">Ver mas</a>
        </div>

        <?php if(($loop->last)||($i==3)): ?>
        </div>
        <hr>
        <?php
        $i=0;    
        ?>
        <?php endif; ?>
        <?php
        $i++;    
        ?>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
      <div class="row off2" style="text-align:center">
          <div class="grid_12"><img src="./images/nada-por-aca.jpg" alt="Sin posteos por el momento">
            <h3>¡Sin Posteos!</h3>
            <p>No hay post por el momento</p>
          </div>
      </div>
      <?php endif; ?>
      
</section>
    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/sitio/resources/views/blog.blade.php ENDPATH**/ ?>
<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Turno;
use Carbon\Carbon;

class ShowTurnos extends Component
{
    public $fecha="hola";
    public function render()
    {   
        $dia= $this->fecha;

       // $turnosReservados=Turno::where('fecha','=',$dia);
       $turnosReservados=array("1"=>"8:00","2"=>"9:15","3"=>"10:00");
        $turnosLibres=array(
            "8:00" => array("estado" => "libre","pausa"=>false),
            "8:15" => array("estado" => "libre","pausa"=>false),
            "8:30" => array("estado" => "libre","pausa"=>false),
            "8:45" => array("estado" => "libre","pausa"=>false),
            "9:00" => array("estado" => "libre","pausa"=>false),
            "9:15" => array("estado" => "libre","pausa"=>false),
            "9:30" => array("estado" => "libre","pausa"=>false),
            "9:45" => array("estado" => "libre","pausa"=>false),
            "10:00" => array("estado" => "libre","pausa"=>false),
        );

        foreach ($turnosLibres as $key => $turno) {
            if (array_search($key,$turnosReservados)) {
                $turnosLibres[$key]['estado']="ocupado";
            }
            
                
            
        }

        return view('livewire.show-turnos',compact('turnosLibres','dia'))
        ->layout('layouts.app');

    }
}

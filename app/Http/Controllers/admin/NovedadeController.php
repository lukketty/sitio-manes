<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\admin\Novedade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class NovedadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $novedades=Novedade::all();
        return view('admin.novedades.index',compact('novedades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.novedades.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'titulo'=>'required',
            'contenido'=>'required',
            'url'=>'required',
            'fecha'=>'required'
        ]);
            
      $novedade= Novedade::Create($request->all());
      if ($request->file('imagen')) {
        $url = Storage::put('Eventos', $request->file('imagen'));
        $novedade->update([
            'imagen' => $url
        ]);
      }
      
       return redirect()->route('admin.novedades.edit', $novedade)->with('info','El Evento se creo con exito!');
    
      
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Novedade  $novedade
     * @return \Illuminate\Http\Response
     */
    public function show(Novedade $novedade)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Novedade  $novedade
     * @return \Illuminate\Http\Response
     */
    public function edit(Novedade $novedade)
    {
        return view('admin.novedades.edit', compact('novedade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Novedade  $novedade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Novedade $novedade)
    {
        $request->validate([
            'titulo'=>'required',
            'contenido'=>'required',
            'url'=>'required',
            'fecha'=>'required'
        ]);
      
        $novedade->update($request->all());
        
        if ($request->file('imagen')) {
            $url = Storage::put('Eventos', $request->file('imagen'));
            $novedade->update([
                'imagen' => $url
            ]);
          }
          
      
       return redirect()->route('admin.novedades.edit', $novedade)->with('info','El evento se actualizo con exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Novedade  $novedade
     * @return \Illuminate\Http\Response
     */
    public function destroy(Novedade $novedade)
    {
        $novedade->delete();
        return redirect()->route('admin.novedades.index')->with('info','Se Elimino el post correctamente');
    }
}

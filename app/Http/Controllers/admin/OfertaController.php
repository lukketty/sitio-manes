<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Oferta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OfertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oferta=Oferta::all();
        return view('admin.ofertas.index',compact('oferta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ofertas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titulo'=>'required',
            'contenido'=>'required',
            'descripcion'=>'required'
        ]);

      $oferta= Oferta::Create($request->all());
      if ($request->file('imagen')) {
        $url = Storage::put('ofertas', $request->file('imagen'));
        $oferta->update([
            'imagen' => $url
        ]);
      }
      
       return redirect()->route('admin.ofertas.edit', $oferta)->with('info','El posteo se creo con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Oferta  $oferta
     * @return \Illuminate\Http\Response
     */
    public function show(Oferta $oferta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Oferta  $oferta
     * @return \Illuminate\Http\Response
     */
    public function edit(Oferta $oferta)
    {
        return view('admin.ofertas.edit', compact('oferta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Oferta  $oferta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Oferta $oferta)
    {
       
        $oferta->update($request->all());
        
        if ($request->file('imagen')) {
            $url = Storage::put('ofertas', $request->file('imagen'));
            $oferta->update([
                'imagen' => $url
            ]);
          }
          
      
       return redirect()->route('admin.ofertas.edit', $oferta)->with('info','El post se actualizo con exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Oferta  $oferta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Oferta $oferta)
    {
        $oferta->delete();
        return redirect()->route('admin.ofertas.index')->with('info','Se Elimino el post correctamente');
    }
}

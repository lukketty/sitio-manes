<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Canale;
use App\Models\Blog;
use App\Models\Oferta;
use App\Models\admin\Novedade;
use App\Models\Medico;
use App\Models\Vademecum;
use Illuminate\Support\Str;


class InicioController extends Controller
{
    
    public function index(){
        
        $urlPubli="https://www.manes.com.ar/mview/";
        $eventos=Novedade::orderBy('fecha', 'ASC')->limit(4)->get();
        $ofertas=Oferta::all();
        
        $fila = Canale::select('*')
                ->join('video', 'lista.id_video', '=', 'video.id_video')
                ->join('canales', 'lista.id_canal', '=', 'canales.id_canal')
                ->where('canales.nro','2')
                ->get();

    	return view ('inicio', compact( 'urlPubli','fila','eventos','ofertas'));
    }
    public function quienes(){
        
        return view('quienes');
    }
    public function pfpc(){
        
        return view('pfpc');
    }
    public function blog(){
        $post=Blog::All();
        
        return view('blog',compact('post'));
    }
    public function nota(Blog $blog){
        $post=Blog::where('id','!=',$blog->id)->take('4')->get();
                
        return view('nota',compact('blog','post'));
    }
    public function contactenos(){
        
        return view('contactenos');
    }
    public function cpms(){
        
        return view('cpms');
    }
    public function cpmsRespuesta(Request $request){
        $dni=$request->dni;
        $medico=Medico::where('NRO_DOC','=',$dni)->get();
        if($medico->isEmpty()){
            $resultado=false;
        } else{
            $resultado=true;
        }
        $respuesta=Vademecum::all();
        
        return view('cpmsRespuesta',compact('respuesta','resultado'));
    }
    public function turnos(){
        
        return view('turnos');
    }
}

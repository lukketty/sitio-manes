<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vademecum extends Model
{
    use HasFactory;
    protected $connection = 'comments';
    protected $table = 'vademecum';
}

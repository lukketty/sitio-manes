<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Novedade extends Model
{
    protected $fillable=['user_id','titulo', 'contenido', 'url','imagen','fecha'];
    
    use HasFactory;
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
}

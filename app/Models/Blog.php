<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable=['user_id','titulo', 'contenido', 'descripcion','imagen'];
    use HasFactory;
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
